package ru.malyshev.HomeWork_4;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Objects;

public class MathBox {
    private ArrayList<Number> numbersArray = new ArrayList<>();


    public  MathBox(Number[] inBoundArray){

        for (Number arrayItem : inBoundArray)
        {
            boolean haveRepeat = false;
            int index=0;

            while ((index < numbersArray.size())&&(!haveRepeat)) {
                if (arrayItem == numbersArray.get(index)) {
                    haveRepeat = true;
                }
                index++;
            }
            if (!haveRepeat) {
                numbersArray.add(arrayItem);
            }
        }
    }

    public Double summator(){
        double sum=0;
        for (Number listItem : numbersArray){
            sum += BigDecimal.valueOf(listItem.doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        }
     return sum;
    }

    public void splitter (Double delimeter) {
        if (delimeter == 0) {
            System.out.println("Деление на ноль запрещено. Обработка данных прервана.");
            return;
        }

        ArrayList<Number> splittedArray = new ArrayList<>();
        for (Number listItem : numbersArray) {
                double addToNewList = listItem.doubleValue() / delimeter;
                splittedArray.add(BigDecimal.valueOf(addToNewList).setScale(2, BigDecimal.ROUND_HALF_UP).floatValue());
        }
            this.numbersArray = splittedArray;
    }

    public String toString() {
        return numbersArray.toString();
    }

    public Double getNumbersArrayItemByIndex(int index){
             return numbersArray.get(index).doubleValue();
           }

    public boolean replaceNumbersArrayItemByIndex(int index,Number numberForReplace){
        if (index < 0|| index > numbersArray.size()) {
            return false;
        } else {
            numbersArray.set(index, numberForReplace);
            return true;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(numbersArray);
    }

    @Override
    public boolean equals(Object obj) {
        // проверяем, что сравниваем именно с MathBox
        if (!(obj instanceof MathBox)) {
            return false;
        }
        MathBox compareMathBox = (MathBox) obj;
        if (this.hashCode() != compareMathBox.hashCode()) {
            return false;
        } else if (this.numbersArray.size() != compareMathBox.numbersArray.size()) {
            return false;
        } else {
          boolean isSame = true;
          int index = 0;
          while (!isSame&&(index < numbersArray.size())){
              if (!numbersArray.get(index).equals(compareMathBox.getNumbersArrayItemByIndex(index))) {
                  isSame = false;
                  index++;
              }
          }
          return isSame;
        }
    }
}
