package ru.malyshev.HomeWork_2.drinks;

public interface Drink {
    double getPrice();
    String getTitle();
}