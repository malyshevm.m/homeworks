package practices.practice6.Comparator;

import java.sql.SQLOutput;
import java.util.Arrays;

public class PersonArray {
    static Person [] array;

    public PersonArray(Person[] array) {
        this.array = array;
    }

    public static void main(String[] args){
        Person person1 = new Person ("Ann",28);
        Person person2 = new Person ("Kate",31);
        Person person3 = new Person ("Lizzy",21);
        Person person4 = new Person ("Mike",25);
        Person person5 = new Person ("Ann",21);

        array = new Person[] {person1,person2,person3,person4,person5};

        System.out.println(Arrays.toString(array));
        Arrays.sort(array,new PersonComparator());
        System.out.println(Arrays.toString(array));



    }
}
