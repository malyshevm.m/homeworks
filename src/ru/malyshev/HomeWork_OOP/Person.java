package ru.malyshev.HomeWork_OOP;

import sun.util.calendar.BaseCalendar;

import java.util.Date;

public class Person {
    private double client_id;
    private String firstname;
    private String surname;
    private String middlename;
    private Date birthday;
    private String secretword;

    public Person(double in_id_client, String in_name,String in_surname, String in_middlename){
        this.client_id = in_id_client;
        this.firstname = in_name;
        this.surname = in_surname;
        this.middlename = in_middlename;
    }

    public void setBirthday(Date b_day){
        this.birthday=b_day;
    }

    public Date getBirthday(){
        return birthday;
    }

    public void setSecretword(String in_secret){
        this.secretword = in_secret;
    }

    public String getClientInfo(){
             return firstname+" "+middlename+" "+surname;
        }

    public double getClientId(){
        return client_id;
    }
    public void showClientSecretInfo(){
        System.out.println(client_id+" secret word is "+secretword);
    }


}



