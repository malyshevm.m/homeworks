package ru.malyshev.HomeWork_4;

import java.math.BigDecimal;
import java.util.Arrays;


public class Main {
    private static final int ARRAY_SIZE = 10;
    public static void main(String[] args){

        Number[] initialArray = new Number[ARRAY_SIZE];
        // Заполняем массив на случайно основе
        for (int i = 0 ; i < ARRAY_SIZE ; i++) {
          switch ((int)(1+Math.round((Math.random())*1)))
          {
              case 1: initialArray[i] = (double)(i+1)*(1+Math.round(Math.random())*99);
                break;
              case 2: initialArray[i] = BigDecimal.valueOf((Math.random())*100).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                break;
          }
        }
        //Выводим первоначальный массив
        System.out.println("Входные данные : "+Arrays.toString(initialArray));
        // 1. Передача массива в MathBox
        MathBox mathBox = new MathBox(initialArray);

        System.out.println("Содержимое коллекции:");
        System.out.println(mathBox.toString());

        // 2. Вызов сумматора
        System.out.println("Сумма элементов массива:" + mathBox.summator());
        // 3. Вызов делителя
        double arrayDelimeter = 0;
        // Проверка деления на ноль
        System.out.println("Операция деления каждого элемента массива на " + arrayDelimeter);
        mathBox.splitter(arrayDelimeter);
        System.out.println("Содержимое коллекции:");
        System.out.println(mathBox.toString());

        arrayDelimeter =  BigDecimal.valueOf((Math.random())*200).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        // Деление на случайное число
        System.out.println("Операция деления каждого элемента массива на " + arrayDelimeter);
        mathBox.splitter(arrayDelimeter);
        System.out.println("Содержимое коллекции:");
        System.out.println(mathBox.toString());


    }
}
