package ru.malyshev.HomeWork_2;


import ru.malyshev.HomeWork_2.drinks.ColdDrink;
import ru.malyshev.HomeWork_2.drinks.Drink;
import ru.malyshev.HomeWork_2.drinks.HotDrink;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner input= new Scanner(System.in);
        //Drinks_enum drink1;
        //drink1 = Drinks_enum.Tea;

        //System.out.println(drink1);
        System.out.println("Добавьте деньги в монетоприемник: (руб.)");
        int moneys = input.nextInt();

        Drink[] hotDrinks = new HotDrink[]{new HotDrink("Чай", 120),new HotDrink("Кофе", 150),new HotDrink("Какао", 170)};
        Drink[] coldDrinks = new ColdDrink[]{new ColdDrink("Кола", 50),new ColdDrink("Пепси", 60),new ColdDrink("Спрайт", 55)};

        VendingMachine vm = new VendingMachine(hotDrinks);

        vm.addMoney(moneys);
        System.out.println("На счету : "+vm.getBalance());
        vm.showContent();

        //vm.giveMeADrink(1);

        vm.setDrinks(coldDrinks);
        vm.showContent();

        //vm.giveMeADrink(0);
    }
 }

enum Drink_type {Cold,Hot}
