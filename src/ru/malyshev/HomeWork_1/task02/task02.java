package ru.malyshev.HomeWork_1.task02;

import java.util.Arrays;
import java.util.Scanner;

public class task02 {
    public static void main(String[] args) {
        int [] numbers= new int[4];
        int minimum;

        Scanner s = new Scanner(System.in);
        for (int i=0; i <4 ; i++){
            System.out.println("Введите целое число:");
            numbers[i] = s.nextInt();
        }
        minimum=numbers[0];

        for (int i=1; i <4 ; i++){
          if (numbers[i] < minimum) {
              minimum=numbers[i];
          }

        }
        System.out.println("Введеные числа: "+ Arrays.toString(numbers));
        System.out.println("Минимальное число: "+minimum);
    }
}
