package ru.malyshev.HomeWork_1.task04;

import java.util.Scanner;

public class task04 {
    public static void main(String[] args) {
        int i;
        System.out.println("Введите целое число:");
        Scanner s = new Scanner(System.in);
        i=s.nextInt();
        System.out.println(i+"!="+factorial (i));
    }

    private static int factorial (int n) {
        if ( n==1) return 1;
        else return n*factorial (n-1);
    }
}
