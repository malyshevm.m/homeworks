package practices.practice6.task3;

public class StackX {
    public int[] array;
    private int size;
    private int pointer;

    public StackX (int size){
        this.size = size;
        this.pointer = -1;
        this.array= new int[size];
    }

    public void peek (int pos){
        System.out.println(pos+"й "+array[pos]);

    }

    public int pop (){
        return array[pointer--];
    }
    public void push (int element){

        if (!isFull())
        { pointer ++;
            array[pointer] = element;}
    }

    public void display (){
        //TODO
        for (int item=0; item < array.length; item++){
            System.out.println(item+"й "+array[item]);
        }
    }

    public boolean isEmpty(){
        return (pointer==-1);
    }

    public boolean isFull(){
        return (pointer == size-1);
    }

    public int getMin(){
        int val = array[pointer];
        for (int item=0; item < array.length; item++) {
            if (array[item] < val) {
                val = array[item];
            }
        }
        return val;
    }

}
