package ru.malyshev.HomeWork_5_2;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class UniqueChars {

    private String text;
    Map<Character,Integer> charFreqList;

    public String getText() {
        StringBuilder rezult = new StringBuilder();
        for (Character key:charFreqList.keySet()){
            rezult.append("'"+key+"' встречается "+charFreqList.get(key)+
                          ((charFreqList.get(key)%10 >=2) && (charFreqList.get(key)%10<=3)  ? " раза" : " раз")+"\n");
        }
        return rezult.toString();
    }

    public void setText(String text) {
        this.text = text;
    }

    public void calculate() {

        charFreqList=new TreeMap<>();

        for (Character letter : text.toLowerCase().toCharArray())
        {
            Integer x = charFreqList.get(letter);
            if (x == null) {
               charFreqList.put(letter,1);
            } else {
               charFreqList.put(letter, x + 1);
            }
        }
    }
}
