package ru.malyshev.HomeWork_OOP;

public class Account {
    private double account_number;
    private double balance;
    private double account_owner;
    private boolean isActive;


    public Account(double in_acc_num){
        this.account_number = in_acc_num;
        this.isActive = false;
    }

    public void setAccountOwner(double client_id)    {
        this.account_number = client_id;
    }

    public double getAccountOwnerId()    {
        return this.account_number;
    }

    public void setBalance (double start_balance){
        this.balance=start_balance;
    }
    public double getBalance (){
        return balance;
    }
    public double getAccountNumber (){
        return account_number;
    }
    public void putMoney (double sum){
        balance+=sum;
    }
    public boolean pullMoney (double sum){
        if (balance-sum < 0 )
         {
             return  false;
         }
        else
          {
            balance=balance-sum;
            return true;}

    }

}
