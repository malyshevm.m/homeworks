package practices.practice6.task3;

import java.util.ArrayList;
import java.util.Arrays;

import static practices.practice6.task3.ReverseArray.put;

public class App {
    public static void main(String[] args) {
        ReverseArray reverseArray = new ReverseArray(10);
        put(1,0);
        put(2,1);
        put(3,2);
        put(4,3);
        put(5,4);
        put(6,5);
        put(7,6);
        put(8,7);
        put(9,8);
        put(10,9);
        reverseArray.display();
        reverseArray.display();
        reverseArray.reverse();
        System.out.println(reverseArray);

    }
}
