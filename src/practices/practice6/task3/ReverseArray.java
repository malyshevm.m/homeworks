package practices.practice6.task3;

public class ReverseArray {
    private static int[] array;
    private int size;
    private StackX stackX;

    public ReverseArray (int size){

        this.size = size;
        array = new int [size];
        stackX = new StackX(size);
    }

    public int[] reverse (){
        //
        int [] reverseArray=new int[size];

        for (int item=0; item < size; item++) {
            stackX.push(array[item]);
        }
        for (int item=0; item < size; item++) {

            reverseArray[item] = stackX.pop();
        }
    return reverseArray;

    }

    public static void put (int item, int index){
        array[index] = item;
    }

    public void display(){

            for (int item=0; item < array.length; item++) {
                System.out.println(item + "й " + array[item]);

            }  //
    }


}
